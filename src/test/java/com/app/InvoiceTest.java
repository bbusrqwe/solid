package com.app;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.app.Invoice;
import com.app.Book;


/**
 * Unit test for simple App.
 */
public class InvoiceTest 
{
    private static final String BOOK_NAME = "Book name";
    private static final String AUTH_NAME = "Author name";
    private static final int    YEAR      = 1975;
    private static final double PRICE     = 20;
    private static final String ISBN      = "ISBN 1212-22-1212";

    private static final int    QUANTITY  = 12;
    private static final double DISC_RATE = 0.10;
    private static final double TAX_RATE  = 0.20;

    private static final double PRICE_WITH_TAXES = (PRICE - PRICE * DISC_RATE) * QUANTITY * (1 + TAX_RATE);
    /**
     * Rigorous Test :-)
     */
    @Test
    public void InvoiceTest()
    {
        Book    book = new Book( BOOK_NAME, AUTH_NAME, YEAR, PRICE, ISBN);
        Invoice inv  = new Invoice(book, QUANTITY, DISC_RATE, TAX_RATE);
        InvoicePrinter invPrn = new InvoicePrinter(inv);
        invPrn.print();
      
        System.out.println("OK");
    }

    @Test
    public void calculateTotalTest()
    {
        Book    book = new Book( BOOK_NAME, AUTH_NAME, YEAR, PRICE, ISBN);
        Invoice inv  = new Invoice(book, QUANTITY, DISC_RATE, TAX_RATE);
        double priceWithTaxes = inv.calculateTotal();
        assertTrue(priceWithTaxes == PRICE_WITH_TAXES);
        System.out.println("OK");
    }


}
