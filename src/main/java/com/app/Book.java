package com.app;

class Book {
  String name;
  String authorName;
  int year;
  double price;
  String isbn;

  public Book(String name, String authorName, int year, double price, String isbn) {
    this.name = name;
    this.authorName = authorName;
    this.year = year;
    this.price = price;
    this.isbn = isbn;
  }
}
